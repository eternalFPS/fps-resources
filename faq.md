# FAQs

People ask the same questions over and over again. Here are a list of the most frequently asked questions (with their responses in copypasta form, so you can just paste them at whoever asked the question)

## I'm new where do I start? / Routines? / Scenarios? / I've been playing nothing but tile frenzy and ascended tracking / ETC.

READ THE AIMER7 GUIDE: [https://www.dropbox.com/s/vaba3potfhf9jy1](https://www.dropbox.com/s/vaba3potfhf9jy1)

## Any questions about the actual aim-trainer itself (e.g. errors/bugs, how to make maps and scenarios, etc)

[https://www.kovaak.com/fpsaimtrainer-faq/](https://www.kovaak.com/fpsaimtrainer-faq/)

## HOW I CONVERT SENSITIVITY FORTNITE??? (or any other game)

[https://www.kovaak.com/fpsaimtrainer-faq/#fortnite](https://www.kovaak.com/fpsaimtrainer-faq/#fortnite)

Specifically for fortnite: [https://www.reddit.com/r/FPSAimTrainer/comments/a0oyc8/fortnite_settings/](https://www.reddit.com/r/FPSAimTrainer/comments/a0oyc8/fortnite_settings/)

## What level should I start at on the AIMER7 guide?

A lot of the levels within the guide actually have scores or at least a description of the requirements for each level. Just start at complete beginner and move to the next level if it feels too easy. If you are still unsure then post a video of your aim and someone will tell you.

## Should I do the click-timing, tracking or complete routine on the guide?

If you can't decide then do complete.

## What are some good scenrios for GAME X

The aimer7 guide is for all games. The game you play isn't any more unique than any of the other games requiring aim. Read the aimer7 guide. If you want a real 1:1 scenario, then just play the game and not kovaaks.

## what is cm? what is this CM thing? What is cm/360?

cm/360 is the standard way to measure your sensitivity. It is bascially the amount your mouse has to move physcially (in cm) for you to do a 360 spin ingame. To calculate your own cm/360, there are many calculators avaliable (ask if you need one), or alternatively you can just whip out a ruler and measure the physcial distance yourself.

As for why people have Xcm in their name, its because some of them started doing it to show off or something, and then people started mocking those people by putting obsurd cm/360 values in their name (e.g. 3000cm, 2.8cm, etc). Just ignore the CM values in people's names as they are either pointless circlejerking or trolling.

## how do I caluculate my cm?

A user (Drimzi) made an online tool that works in your browser: [https://jscalc.io/calc/za5TQmMatqU4kXSR](https://jscalc.io/calc/za5TQmMatqU4kXSR)  
Kovaak has a standalone tool, however it requires downloading: [https://www.kovaak.com/sensitivity-matcher/](https://www.kovaak.com/sensitivity-matcher/)  
Mouse-sensitivity is online and very featureful, however some calculations require a premium account: [https://www.mouse-sensitivity.com/](https://www.mouse-sensitivity.com/)  
And generally you should be able to find a calculator if you just search for something like "fortnite sensitivity calculator"

## Should I use the same settings in kovaaks that I use ingame?

yes