Below is a list of routines that are not from AIMER7's Kovaak routine book. I've tried to include the author, but in some cases I couldn't remember the author at all.  
Also, they are kinda less of routines and more just like a list of good senarios. If you want to make it a scenario, choose like 6ish, 1 target acquistion and then the rest a mix of click timing and tracking, and do each for 10 mins.

Most of these were originally images, but I converted them into text.

## forest

Not sure of the original purpose, however I think it was Counter Strike related, but it may have been non-game-specific. It is broken down into 3 sections; flicks, muscle memory and tracking.

### flicks

* 1wall5targets_pasu
* 1wall 6targets small
* 1wall6targets

### muscle memory

* bounce 180 tracking
* fuglaaXYlongstrafesHARD
* flicktrack - bounce (press 2 to switch to tracking gun)

### tracking

* popcorn
* thin strafes
* cata ic strafes long invincible

## fuglaa

* Thin Aiming Long -> precise smooth long movement
* Thin Aiming Short -> reactivity + precision
* Air Precise -> general xy tracking
* Close Fast Strafes -> fast reaction
* SmoothBot -> arm + wrist transitions
* Reflex Flick -> general flicking
* Tile Frenzy -> Building CPS+mousing speed
* 1w6t Precise -> Precision + speed

## ZCR

* Air
* FuglaaXYShortStrafes
* Thin Aiming Long
* INCR
* Cata IC Fast Strafes
* LG Pin 360
* mgliscan
* RexStrafesCata
* Ground Plaza
* Cata IC Long Strafes
* Close Fast Strafes Invincible
* Ascended Tracking v2

## ??? (unknown author, potentially apa3?)

comment included within image: **ascended tracking sucks**

* popcorn/popcorn nightmare
* 1wall5targets_pasu
* 1wall6targetste
* tile frenzy/tile frenzy mini/tile frenzy standard 02 (play these if you have autism)
* thin aiming long
* close long/fast strafes
* lg pin practice
* air
* ground plaza
* t1 drop punish 1 side
* caged orb tracking