# I don't have a good name for this at all. I can't think of a good name. Please suggest a better name.

As you play kovaaks, you tend to have a lot of "free space" to think. Aiming in itself is not so much a mentally challenging task, especially in comparison to actual games where there are highly complex enemys, in a very complex world. You constantly have to think about your enemies decisionmaking and what they are thinking, as well as keeping track of everything else happening in the game (e.g. pickups, zone coming in, materials, the clock, cooldowns, etc).  
In kovaaks, the entire game is reduced down to such a state that you barely need any real thought process, and your enemies are literally 0 iq.  
This results in your mind being "free" to think about whatever it wants.

However, with this freedom, I find that unless it is controlled, I tend to do shit and barely improve.  
What really helps me, is if I am given something to focus on while aiming.  
for example; smoothness. If I focus on trying to make my movement as smooth as possible, I find that I am move involved in not just smooth mouse movements, but the entire aiming proccess.  
However, if I am just left to think about nothing, I find myself thinking about other things which do not concern aiming, which casuses me to aim like shit and bascially waste my time.

Below you will find a bunch of "tricks" or "tips" or "aiming philosophy", that I consiously focus on while aiming. I've tried to include the names of the people I have taken these from, however in some cases I may have credited the wrong person.

## (Inspired by Kovaak's timescaling approach)

1. Keep decreasing timescale by 0.05 until you stat hitting a score that is halfway between your PB and the WR (make sure the WR isn't cheated).  
2. Then increase by 0.05.  
3. When you can again hit the score that is halfway between your pb and wr, increase the timescale by 0.05 again.
4. Repeat until you are back at 1.0 timescale.

## [Aim Theory Google Doc](https://docs.google.com/document/d/1VZazZ9ZWbwdhwjNp3p797vR2G24aRHOQNhnLTbngvMA)

### Click Timing

Approach smoothly. I think of this process as [easing](https://www.youtube.com/watch?v=p93K_TtQfiI)  
Pace/time your shots (may be useful to use a metronome)  
Build up speed (start slow but accelerate)  
Have the next target in mind

### Tracking

Be smoother  
Focus entirely on target. No attention given to crosshair  
Always have next target in mind (more of a target aquisition tip)

## [Nut](https://github.com/nut-kovaaker/kovaaks-tracking-guide)

### Beginner

Match movement speed smoothly  
No predicting, react only  
flick back onto center of mass whenever it changes direction  

### Intermediate

Move your arm in the opposite direction you turn your mouse.  
Only use the wrist to reposition (when at full extension, just lift up mouse and straighten ONLY the wirst).  
Flick on target faster when it strafes.  
Turn crosshair on and off.  
Experiment/focus on different grips and positions.  

### Advanced

Flick faster.  
less of a focus on being smooth (you should have that down by now).  
Put crosshair on the "tail" of its movement (crosshair has "lag" tracking the target).  
Tense your arm up a bit more.  
Really focus on the score.  

## Ryan

Focus on moving in the same direction.  
Focus on having the same speed as the bot.  
Focus on being smoother.

## Bozott

1. Look at the 10 people above you, average out their accuracy.  
2. If their accuracy is higher than yours, focus on your accuracy.  
3. If their accuracy is lower than yours, focus on being faster.  

## Busdriverx

Focus on moving as little as possible. Move only as much as is needed (note: this isn't "be slow" its "take the path that requires the least distance travelled")

## -

Focus only on the HEAD of the target (or the top part if it is headless).

## sini

Crosshair off, Crosshair on.