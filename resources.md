The purpose of this guide is (mostly) to serve as a directory/listing of various guides and resource related to FPS games, however some guides may be more focused on general competitive gaming and even real life. They should all benifit FPS games however.

Majority of these links/resources are taken from [Sparky Aim](https://discord.gg/cXQbNSE). This is just a neater presentation rather than pinned discord messages.

ALSO NOTE: Obvious guides/resources are not included. Anything that could be found on the first google search is not here.  
Bad guides are also not included. Hence why you don't see much reddit nor youtubers here, because they suck.

Key:  
**Bold** items are deemed very important/useful.

## [**Most important vid**](https://www.youtube.com/watch?v=SlCRfTmBSGs)

## Topics

### General Competitive Gaming
* [**eAthlete Labs**](https://www.youtube.com/channel/UC2JYwDR4NeGqMGXOkB-Ljpg/videos)
* [Skyline's Youtube](https://www.youtube.com/channel/UCDrfc_LU0llSRI7icBFoBnw/videos) Newer videos
* [**How to improve in eSports**](https://www.reddit.com/r/starcraft/comments/8xm2fr/a_comprehensive_guide_for_improving_at_starcraft_2/)
* [DDK's podcast](https://www.youtube.com/watch?v=SIOEi7cx3J4)
* [Complexity's eSports Evolved](https://www.youtube.com/playlist?list=PLY4JacIIesa60ptYZf8Cx22uLYXF0bUjF)

---

### General FPS
* [AIMER7's Positioning Guide](https://www.dropbox.com/s/aif0jy1prxe0rjm/Heuristic%20about%20geometric%20positioning.pdf?dl=0) - Very deep.
* [FPS Coach](https://www.youtube.com/channel/UCr97_mhDwscRciN6eWyXLow/videos) New channel. Mostly related to CSGO.

---

### Aim

#### Guides
 * ## [**AIMER7's Aim Guide/Routines**](https://www.dropbox.com/s/vaba3potfhf9jy1/KovaaK%20aim%20workout%20routines.pdf?dl=0) Most important.
 * [Sparky Community Tips](https://docs.google.com/document/d/1bWnBGfPzgGsNRmKEve6wxlXSiQYjVg5srsNX0Hlvv2Q/edit#heading=h.n428okhncadn)
 * [nut's Tracking Guide](https://github.com/nut-kovaaker/kovaaks-tracking-guide/blob/master/README.md)
 * [AIMER7's Strafe Aiming 101](https://www.dropbox.com/s/sggvgbwpz9e5bih/Strafe%20Aiming%20101.pdf?dl=0)

#### Communities
* [**Kovaak's FPS Aim Trainer**](https://discord.gg/asX3cna) Best overall discord
* [Yuki Aim](https://discordapp.com/invite/U3UhZDj) Most popular aiming group.
* [Japanese Aiming discord](https://discord.gg/4s5gGYH) Has weekly competitions
* [Sparky Aim](https://discord.gg/cXQbNSE) Sparky is a community based around self improvement, primarly in fps/shooters but not exclusively.
* [vF Discord](https://discord.gg/v9Hv8qr) Another aiming clan. Bascially just the old kovaaks discord (a lot of the discussion moved from there to here).

---

### Games

#### Fortnite

##### Guides
* [r/fnc From Bot to Tryhard Guide](https://old.reddit.com/r/FortniteCompetitive/comments/9naygv/from_bot_to_tryhard_sweat_the_unofficial/)

##### Tools/Other
* [Practice Courses](https://www.reddit.com/r/FortniteCompetitive/comments/aal40k/creative_list_of_practice_courses_megathread/)
* [**Fortnite Esports Wiki**](https://fortnite-esports.gamepedia.com/Fortnite_Esports_Wiki)

---

#### Overwatch

##### Guides
* [Aimer7's Tracer Guide](https://www.dropbox.com/s/nh3lgbwf2teyux2/Exhaustive%20Tracer%20guide.pdf?dl=0) - Outdated, but still very useful.

##### Viewing
* [Jayne's OLDER videos](https://www.youtube.com/channel/UCMoNOUJPZTjA1w3ttT819SA/videos?view=0&sort=da&flow=grid) His older videos are much more useful.
* [Skyline's Older Videos](https://www.youtube.com/channel/UCDrfc_LU0llSRI7icBFoBnw/videos?view=0&sort=da&flow=grid) New videos are good, but only his older videos have overwatch focus.

##### Tools/Other
* [Overbuff](https://www.overbuff.com/heroes) Best stat page. Use the heros page to determine MM meta (use "this week" and "Grandmaster" filters)
* [OmnicMeta](https://www.omnicmeta.com/owl/meta/) Shows OWL meta.
* [OmniCoach](https://omnicoach.gg/offer-site/) AI that will auto vod-review your vods. Paid service
* [Winston's Lab](https://www.winstonslab.com/) Was an insanely good stats site. No longer getting updated.
* [**Overwatch Liquipedia**](https://liquipedia.net/overwatch/Main_Page)

---

#### Counter Strike: Global Offensive (CSGO)

##### Guides
* [Sparky's Counterstrike Community Tips](https://docs.google.com/document/d/1LzIEaJZ-0GGqWmFKE0sGOLmFXMQPs_sJ3dts1ogdOJw/edit)

##### Viewing
* [**Old Netcodeguides**](https://www.youtube.com/user/NetcodeIlluminati/videos) Theres a million videos here, (almost) all useful.

##### Tools/Other
* [Better hud](http://www.spddl.de/csgo_english-txt/csgo_multi-txt)
* [**Recent Pro Demos**](https://www.hltv.org/results?content=demo)

---

#### Apex Legends

##### Guides
* [hamfz's Competitive Apex Legends Guide](https://docs.google.com/document/d/11-G8P-_YAX7sh_FK409GTOuE1qIGF5kPtfz2pfgx5cY/edit#bookmark=id.l8y7zlw88rqy) Unfinished and slightly outdated

##### Tools/Other
* [**Minimal Apex Legends Config**](https://github.com/NotTsunami/ApexConfig) Best config files. Constantly updated and tournament legal
* [**Apex Legends Liquipedia**](https://liquipedia.net/apexlegends/Main_Page)

---

#### Other Games
* [Sparky's Call of Duty (COD) community tips](https://docs.google.com/document/d/1F-SWHjJj-8mfzfkgD5cP9VlHXrbvHJwmKIttxsb5e5A/edit)
* [Sparky's Destiny community tips](https://docs.google.com/document/d/1rOUCeCtRUR8CXEzKn610P1W1IFyZxf8MZ6FefmhU9wc/edit)
* [Sparky's Halo community tips](https://docs.google.com/document/d/1gevT9_G3MOWs0j8acLDjkVdgp6svQZcgIYYhlA43mhc/edit#heading=h.hvc3pakix857)