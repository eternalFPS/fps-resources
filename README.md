Welcome to eternal.

Here you can find a bunch of different items relevant to competitive FPS games.

## Currently a work in progress.

Our [Resources](resources.md) page contains a bunch of useful resources in a number of different categories (all external references). For example, general competitive gaming guides, aiming communities, tools for games, etc.

Our [Routines](routines.md) page covers a bunch of alternate routines, for people too bored of AIMER7's or wanting some more variation.

The [FAQ](faq.md) goes over some of the most commonly asked questions that everyone seems to keep on asking. The answers are in a copypasta format so you can easily respond to people asking about fortnite sensitivites for the 47th time today.

[Aimgroups](aimgroups.md) page contains a table of all the different aiming groups (e.g. vF, yuki, sparky, etc)

The [Aimthinking](aimthinking.md) page goes over a bunch of different ideas you can focus on while aiming (good if you find yourself otherwise getting distracted and going onto autopilot).